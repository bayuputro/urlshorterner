<?php

/**
 * index.php
 * 
 * Base class for all controller
 * @author Bayu Putra <bayuputra_mail@yahoo.com>
 * @version 1.0
 * @package 
 */
 
spl_autoload_extensions(".php");
spl_autoload_register();

include 'system/bootstrap.php';

system\classes\Config::getInstance()->setFile('config.php');

try{
	init();
}catch(system\classes\ControllerNotExistException $e){
	echo $e->getMessage();
}catch(Exception $e){
	echo $e->getMessage();
}





