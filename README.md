# URL Shortener #

### Demo ###
http://bayuputra.info/portfolio/urlshortener/

### Libraries ###
* jQuery version 2.0.0

### Description ###
* The application is build based on framework that used to build JSON Resume project (https://bitbucket.org/bayuputro/json-resume)

### How To Deploy ###
* Clone the repository
* Create config.php file from config.sample.php
* Deploy sql dump database.sql