<?php

/**
 * index.php
 * 
 * Bootstrap to route request to proper controller
 * @author Bayu Putra <bayuputra_mail@yahoo.com>
 * @version 1.0
 * @package 
 */
 use system\classes\ControllerNotExistException;
 use system\classes\Config;
 
 function init(){
	 $request = get_request_uri();
	 
	 //if controller defined
	 if($request){
		 $controller_name = controller_path_to_name(array_shift($request));
		 
		 //if method defined
		 if(count($request)){
			$method = array_shift($request);
			
			//if parameter defined
			$parameters = [];
			if(count($request)){
				
			}else{
				
			}
		 }else{
			$method = "index";
		 }
	 }else{
		 $controller_name = controller_path_to_name(Config::getInstance()->get('default_controller'));
         $method = "index";
	 }
	 
	 route($controller_name,$method);
	 
	 
 }
 
 function get_request_uri()
 {
	
     if(!isset($_SERVER['PATH_INFO'])) return false;
     
     $uri = explode('/',$_SERVER['PATH_INFO'] );
	 array_shift($uri);
	 
	 return isset($uri) ? $uri : false;
	 
 }
 
 function controller_path_to_name($path){
	 return 'controllers\\'.ucfirst($path)."Controller";
 }
 
 function controller_exist($controller){
	return file_exists($controller);
 }
 
 function route($controller,$method){
	 
	 //initialize targeted controller
	if(!controller_exist(Config::getInstance()->get('base_path').$controller.'.php')) 
		throw new ControllerNotExistException();
	 
	$active_controller = new $controller();
	$active_controller->$method();
	 
	
 }
 
 